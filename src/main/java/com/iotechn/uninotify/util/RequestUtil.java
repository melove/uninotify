package com.iotechn.uninotify.util;

import com.iotechn.uninotify.domain.DeveloperDO;

/**
 * Created with IntelliJ IDEA.
 * Description: 调用上下文工具
 * User: rize
 * Date: 2019/12/26
 * Time: 17:37
 */
public class RequestUtil {

    private static ThreadLocal<DeveloperDO> threadLocal = new ThreadLocal();

    public static void setDeveloper(DeveloperDO developer) {
        threadLocal.set(developer);
    }

    public static DeveloperDO getDeveloper() {
        return threadLocal.get();
    }

}
